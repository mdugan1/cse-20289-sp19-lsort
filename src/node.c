/* node.c: Node Structure */

#include "lsort.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/**
 * Create node. 
 *
 * This allocates and configures a node that the user must later deallocate.
 * @param   string      String value.
 * @param   next        Reference to next node.
 * @return  Allocated node structure.
 */
Node *node_create(char *data, Node *next) {
    Node* n = calloc(1,sizeof(Node));
    if(!n){
        return NULL; //failure to allocate memory
    }
    char * dataOnHeap = strdup(data);
    //assign the value of next and correct the node's pointer to data
    
    n -> data = dataOnHeap;
    n -> next = next;
    return n;
}

/**
 * Delete node.
 *
 * This deallocates the given node (recursively if specified).
 * @param   n           Node to deallocate.
 * @param   recursive   Whether or not to deallocate recursively.
 * @return  NULL pointer.
 */
Node *node_delete(Node *n, bool recursive) {
    if(recursive){
        //this iteratively deletes all the nodes when recursive is true
        Node* next = n->next;
        while(next!=NULL){
            next = n -> next; //store the next node
            free(n->data); //free the string
            free(n); //free the node itself
            n = next; //iterate to the next node
        }
    } else{
        free(n->data); //free the data
        free(n); //free the node
    }
    return NULL;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
