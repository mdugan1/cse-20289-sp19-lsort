/* list.h: List Structure */

#include "lsort.h"

#include <stdlib.h>
#include <string.h>

/* Internal Functions */

Node *  node_reverse(Node *curr, Node *prev);
Node *  list_merge_sort(Node *head);
void    list_split(Node *head, Node **left, Node **right);
Node *  list_merge(Node *left, Node *right);

/* External Functions */

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
List *  list_create() {
    List * l = calloc(1,sizeof(List)); //creates a list with everything initialized to zero
    return l;
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l       List to deallocate.
 * @return  NULL pointer.
 */
List *  list_delete(List *l) {
    l->head = node_delete(l->head,true); //free all the nodes in the list using recursive node delete; l->head will be null at end of line
    l->tail = NULL; //set the tail pointer to null to erase the reference to it
    free(l); //free the list itself
    return NULL;
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l       List structure.
 * @param   s       String.
 */
void    list_push_back(List *l, char *s) {
    if(l->size){ //if the list is not empty, push stuff ordinarily using the tail
        Node *n = node_create(strdup(s),NULL); //create a node with data the same to that of s and with the next pointer to null
        l->tail->next = n; //set the (current) tail's next pointer to n
        l->tail = n; //fix the tail, so that n is the tail
        l->size ++;
    } else { //if the list is empty, set everything
        Node *n = node_create(strdup(s),NULL);

        //we just make a list of size 1 with head and tail equal to n
        l->head = n;
        l->tail = n;
        l->size++;
    }
    return;
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l       List structure.
 */
void    list_reverse(List *l) {
    l->tail = l->head; //the head will become the tail
    l->head = node_reverse(l->head,NULL); // reverse entire list
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
Node *  node_reverse(Node *curr, Node *prev) {
    Node *n = curr;
    if(curr->next){
        n = node_reverse(curr->next,curr);
    }
    curr->next = prev;
    return n;
}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l       List structure.
 */
void  list_sort(List *l) {
    
    l->head = list_merge_sort(l->head); //get new head
    //get new tail
    while(l->tail->next){ //iterate through until you get to the end
        l->tail = l->tail->next;
    }
    
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @return  The new head of the list.
 */
Node *  list_merge_sort(Node *head) {
    Node * left = NULL;
    Node * right = NULL;
    Node * newHead =  NULL;

    if(head->next == NULL){ //one lonesome node
        return head;
    } else if(!head){ //empty list/empty node
        return NULL;
    }
    
//    printf("splitting\n");
    list_split(head, &left, &right); //split into left and right
  //  printf("sorting left list\n");
    left = list_merge_sort(left); //set the left node, reursively
    //printf("sorting right list\n");
    right = list_merge_sort(right); //set the right, recursively
//    printf("giving new head by merging\n");
    newHead = list_merge(left,right); //merge to get new head
  //  printf("returning new head by merging\n");
    return newHead; 
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void    list_split(Node *head, Node **left, Node **right) {
    //we will use the fast-slow trick to do this
    Node * fast = NULL;
    Node * slow = NULL;
    
    //check for base cases
    if(!head){ //if the list is empty, return
        return;
    } else if(head->next == NULL){ //otherwise, if the list is one-long, no need to split
        return;
    } else { //fast-slow method
        fast = head;
        slow = head;
        while(fast && fast ->next){ //while fast && its next (short circuit avoids seg fault)
            slow = slow->next; //increment slow by 1 nodes
            fast = fast->next->next; //increment fast by 2 nodes
        }
        *left = head; //beginning of left is head, as before

        while(head->next != slow){ //we are going to iterate head to slow
            head = head ->next;
        }
        head->next = NULL; //set head's next to null, ending the list

        *right = slow; //beginning of right list will be equal to slow; no need to find its tail, since its next will already be NULL
    }
    
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @return  The new head of the list.
 */
Node *  list_merge(Node *left, Node *right) {
    Node *tail = NULL;

    
    //set tail
    int cmp = strcmp(left->data,right->data);
    if(cmp <0){
        tail = left;
        left = left->next;
    } else if (cmp == 0){
        tail = left;
        left = left ->next;
    } else{
        tail = right;
        right = right->next;
    }

    Node * head = tail; //makes the head pointer to be returned


    while((left && right)){
        cmp = strcmp(left->data,right->data);
        if(cmp>0){ //left string greater than right string
            tail->next = right;
            tail = right;
            right = right->next;
        } else {
            tail->next = left;
            tail = left;
            left = left ->next;
        }
    }

    if(!left){
        tail ->next = right;        
    }
    if(!right){
        tail ->next = left;
    }

    return head;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
